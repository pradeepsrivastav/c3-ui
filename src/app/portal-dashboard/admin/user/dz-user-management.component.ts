import {Component} from "@angular/core";
import {MatDialog} from "@angular/material";
import {UserService} from "./user.service";
import {OrganizationService} from "../../../services/organization.service";
import {RoleService} from "../../../services/role.service";
import {DzEditUserComponent} from "./dz-edit-user/dz-edit-user.component";
import {DzBatchCreateUsersComponent} from "./dz-batch-create-users/dz-batch-create-users.component";
import {AuthHolderService} from "../../../services/auth-holder.service";
import {Observable} from "rxjs";
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/models/role.model';
import { Permission } from 'src/app/models/permission.model';
import { PageRequest } from 'src/app/models/page-request.model';
import { Page } from 'src/app/models/page.model';
import { AbstractManagementTableComponent } from 'src/app/shared/abstarct-management-table.component';


@Component({
    templateUrl: "./dz-user-management.component.html"
})
export class DzUserManagementComponent extends AbstractManagementTableComponent<User> {
    columns = ["firstName", "lastName", "email", "organization", "role", "mobilePhone", "workPhone", "pin", "actions"];
    protected editDialogComponent = DzEditUserComponent;

    readonly: boolean;
    authSubordinateRoles: Role[];

    constructor(dialog: MatDialog,
                public userService: UserService,
                public organizationService: OrganizationService,
                public roleService: RoleService,
                public auth: AuthHolderService) {
        super(dialog);

        this.readonly = !auth.hasPermission(Permission.EDIT_USERS);
        this.readonly = false;
        if (this.readonly) {
            this.removeColumn("actions");
        }
        if (auth.isOrganizationAdmin()) {
            this.removeColumn("organization");
        }
        roleService.getSubordinateRoles().subscribe(roles => this.authSubordinateRoles = roles);
    }

    sendPin(user: User) {
        this.userService.sendPin(user.id).spinner().subscribe();
    }

    openBatchCreateUserDialog() {
        this.dialog.open(DzBatchCreateUsersComponent, {width: "450px"}).afterClosed()
            .subscribe(result => result && this.dataSource.reload());
    }

    protected getData(pageRequest: PageRequest, filters: any): Observable<Page<User>> {
        return this.userService.getUsers(pageRequest, filters);
    }

    protected deleteElement(element: User): Observable<any> {
        return this.userService.deleteUser(element.id);
    }
}
