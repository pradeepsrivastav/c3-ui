import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {OrganizationService} from "../../../../services/organization.service";
import {UserCreateUpdateRequest, UserService} from "../user.service";
import {Role} from "../../../../models/role.model";
import {RoleService} from "../../../../services/role.service";
import {AuthHolderService} from "../../../../services/auth-holder.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DzValidators} from "../../../../common/dz-validators";
import {User} from "../../../../models/user.model";

@Component({templateUrl: "./dz-edit-user.component.html"})
export class DzEditUserComponent {

    userForm: FormGroup;
    organizationRoles: Role[] = [];
    public user: User;
    constructor(
                public organizationService: OrganizationService,
                public userService: UserService,
                public roleService: RoleService,
                public auth: AuthHolderService,
                public formBuilder: FormBuilder,
                public activatedRoute: ActivatedRoute,
                private router: Router
               ) {

           let id = this.activatedRoute.snapshot.params.id;
           console.log(id);
           if(id){
            // this.initUser();
            this.userService.getUser(+id).subscribe(user =>{
              this.user = user;
              this.initUser();
            },err=>{
              alert('err');
              this.initUser();
            })
           }else{
             this.initUser()
           }
    }

    initUser(){
      let uniqueEmailValidator = DzValidators.asyncUnique(email => this.userService.checkEmailUnique(email), this.user && this.user.email);
        this.userForm = this.formBuilder.group({
            firstName: [this.user && this.user.firstName],
            lastName: [this.user && this.user.lastName],
            email: [this.user && this.user.email, [], uniqueEmailValidator],
            organization: [this.user && this.user.organization],
            role: [this.user && this.user.role],
            mobilePhoneNumber: [this.user && this.user.mobilePhoneNumber],
            workPhoneNumber: [this.user && this.user.workPhoneNumber],
        });

        if (this.auth.isOrganizationAdmin()) {
            this.updateOrganizationRoles(this.auth.getAuthentication().organizationType);
        } else if (this.user) {
            this.updateOrganizationRoles(this.user.organization.type);
        }
    }

    saveUser() {

        let form = this.userForm.value;
        console.log(form);
        let createUpdateRequest: UserCreateUpdateRequest = {
            firstName: form.firstName,
            lastName: form.lastName,
            email: form.email,
            organizationId: this.auth.isOrganizationAdmin()
                ? this.auth.getAuthentication().organizationId
                : form.organization.id,
            role: form.role.name,
            mobilePhoneNumber: form.mobilePhoneNumber,
            workPhoneNumber: form.workPhoneNumber
        };
        let request$ = this.user
            ? this.userService.updateUser(this.user.id, createUpdateRequest)
            : this.userService.saveUser(createUpdateRequest);

        request$.spinner().subscribe(user => console.log('save user'));
    }

    updateOrganizationRoles(organizationType: string) {
        return this.roleService.getRolesAvailableForOrganizationType(organizationType).subscribe(roles => this.organizationRoles = roles);
    }

    cancelStep() {
     this.router.navigate(['../configure'],{relativeTo: this.activatedRoute});
    }
}
